import * as cesium from "cesium"
import "cesium/Build/Cesium/Widgets/widgets.css";

//cesium.Ion.defaultAccessToken = import.meta.env.VITE_CESIUM_TOKEN
// cesium.Ion.defaultAccessToken =
//   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJkOTk5Y2ZiNi1iZTFlLTRhZTQtYmUzZS04YWRiNDU1YzM5ZDciLCJpZCI6MTc4Nzk1LCJpYXQiOjE3MDAxNDQ3MzZ9.-zAKfgbNy81ZZzEn12l81D1Lr65_y5dz5vAEvA3F-AE";
export const options: cesium.Viewer.ConstructorOptions = {  //cesium的属性
  // infoBox: false,
  // selectionIndicator: false,
  // animation: false,
  // //baseLayerPicker: false,
  // geocoder: false,
  // navigationHelpButton: false,
  // fullscreenButton: false,
  // homeButton: false,
  // //sceneModePicker: false,
  // timeline: false,
  // shadows: true,
  // shouldAnimate: true

  animation: false, //隐藏动画控件
  baseLayerPicker: false, //隐藏图层选择
  fullscreenButton: false, //隐藏全屏
  geocoder: false, //隐藏查找控件
  homeButton: false, //隐藏视角返回初始位置按钮
  //timeline: false, //隐藏时间线控件
  navigationHelpButton: false, //隐藏帮助
  navigationInstructionsInitiallyVisible: false, //隐藏导航说明
  sceneModePicker: false, //隐藏视角模式3D 2D CV
  creditContainer: document.createElement("div"), //隐藏logo

  scene3DOnly: true, //当 true 时，每个几何实例将仅以 3D 呈现以节省 GPU 内存
  skyBox: false, //天空盒用于渲染星星。当 undefined 时，使用默认的星星。如果设置为 false ，则不会添加天空盒、太阳或月亮
  skyAtmosphere: false, //蔚蓝的天空，以及围绕着地球四肢的光芒。设置为 false 将其关闭
  useDefaultRenderLoop: true, //如果此小部件应控制渲染循环，则为 true，否则为 false
  requestRenderMode: true, //请求渲染模式
  selectionIndicator: true, //原生自带绿色选择框，双击显示的绿框
  infoBox: false, //隐藏点击要素之后显示的信息窗口
  vrButton: false, //隐藏VR按钮，默认false
  shouldAnimate: true, //true 时钟应该默认尝试提前模拟时间，则为 true，否则为 false
  sceneMode: 3, //初始场景模式 1：2D 2：2D循环 3：3D，默认3
  mapProjection: new cesium.WebMercatorProjection(), //地图投影体系，默认Ellipsoid.WGS84
  shadows: false, //确定阴影是否由光源投射,默认false
}

export function useCesium(element: HTMLElement) {


  cesium.Ion.defaultAccessToken =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJkOTk5Y2ZiNi1iZTFlLTRhZTQtYmUzZS04YWRiNDU1YzM5ZDciLCJpZCI6MTc4Nzk1LCJpYXQiOjE3MDAxNDQ3MzZ9.-zAKfgbNy81ZZzEn12l81D1Lr65_y5dz5vAEvA3F-AE";

  const viewer = new cesium.Viewer(element, {
    ...options,
    //terrainProvider: cesium.createWorldTerrain(), //老版本的，不能用了
    terrain: cesium.Terrain.fromWorldTerrain({
      requestWaterMask: true, //指示客户端是否应从服务器请求额外的照明信息
      requestVertexNormals: true, //指示客户端是否应从服务器请求每个瓷砖水面具
    }), // 地形数据,离线需要自己提供
  })

  viewer.scene.globe.depthTestAgainstTerrain = true
  //清除版权信息
  // const creditContainer = viewer.cesiumWidget.creditContainer as HTMLElement
  // creditContainer.style.display = "none"

  return { viewer }
}
