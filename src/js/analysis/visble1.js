
//通视分析
var modelposition = Cesium.Cartesian3.fromDegrees(obj.position[0], obj.position[1], obj.position[2]);
var heading = Cesium.Math.toRadians(obj.heading);
var pitch = Cesium.Math.toRadians(obj.pitch);
var roll = Cesium.Math.toRadians(obj.roll);
var hpRoll = new Cesium.HeadingPitchRoll(heading, pitch, roll);
var converter = Cesium.Transforms.eastNorthUpToFixedFrame;
var modelMatrix = Cesium.Transforms.headingPitchRollToFixedFrame(modelposition, hpRoll, Cesium.Ellipsoid.WGS84, converter);
tsdyt_model = viewer.scene.primitives.add(
    new Cesium.Cesium3DTileset({
        url: "http://earthsdk.com/v/last/Apps/assets/dayanta/tileset.json",
    })
);
tsdyt_model.readyPromise.then(function (tsdyt_model) {
    //请求模型后执行
    tsdyt_model._root.transform = modelMatrix; //模型位置
}).otherwise(function (error) {
    throw error
})
viewer.zoomTo(tsdyt_model)

var CesiumEventHandler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
var positions = [];
var markers = [];//点实体
CesiumEventHandler.setInputAction(function (movement) {
    var cartesian = viewer.scene.pickPosition(movement.position);
    if (cartesian) {
        positions.push(cartesian);//加点
        if (markers.length == 0) {
            //创建点实体
            var startpoint = viewer.entities.add({
                position: cartesian,
                billboard: {
                    image: obj.startpointStyle.image,
                    heightReference: Cesium.HeightReference.NONE
                },
                label: {
                    text: obj.startpointStyle.text,
                    fillColor: Cesium.Color.YELLOW,
                    pixelOffset: {
                        x: obj.startpointStyle.pixelOffsetX,
                        y: obj.startpointStyle.pixelOffsetY
                    },
                    scale: obj.startpointStyle.scale
                }
            });
            markers.push(startpoint);
        }
        else if (markers.length == 1) {
            var endpoint = viewer.entities.add({
                position: cartesian,
                billboard: {
                    image: obj.endpointStyle.image,
                    heightReference: Cesium.HeightReference.NONE
                },
                label: {
                    text: obj.endpointStyle.text,
                    fillColor: Cesium.Color.YELLOW,
                    pixelOffset: {
                        x: obj.endpointStyle.pixelOffsetX,
                        y: obj.endpointStyle.pixelOffsetY
                    },
                    scale: obj.endpointStyle.scale
                }
            });
            markers.push(endpoint);
            CesiumEventHandler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK)//移除左键事件
            analysisVisible(positions);//开始分析
        }
    }
}, Cesium.ScreenSpaceEventType.LEFT_CLICK);


// * 进行通视分析
function analysisVisible(positions) {
    // 计算射线的方向
    let direction = Cesium.Cartesian3.normalize(
        Cesium.Cartesian3.subtract(
            positions[1],
            positions[0],
            new Cesium.Cartesian3()
        ),
        new Cesium.Cartesian3()
    );
    // 建立射线
    let ray = new Cesium.Ray(positions[0], direction);
    // 计算交互点，返回第一个
    let result = viewer.scene.pickFromRay(ray);
    // console.log(result)
    if (Cesium.defined(result) && Cesium.defined(result.object)) {
        drawLine(result.position, positions[0], Cesium.Color.GREEN); // 可视区域 
        drawLine(result.position, positions[1], Cesium.Color.RED); // 不可视区域
    }
    else {
        drawLine(positions[0], positions[1], Cesium.Color.GREEN);
        console.log("不在模型上")
    }
}



/////////////////////////////////////////////////////////////

private sightline(startWorldPoint: Cesium.Cartesian3,
    endWorldPoint: Cesium.Cartesian3): Cesium.Cartesian3{
    let barrierPoint: Cesium.Cartesian3 = Cesium.Cartesian3.ZERO;
    // const startWorldPoint = pickCartesian(this.viewer,startPoint).cartesian;
    // const endWorlePoint = pickCartesian(this.viewer,endPoint).cartesian;
    // console.log("start:" +startWorldPoint )
    // console.log("end:" +endWorlePoint )
    const startPoint = convertCartesian3ToCartesian2(this.viewer, startWorldPoint);//将Cartesian3转换为windows坐标
    const endPoint = convertCartesian3ToCartesian2(this.viewer, endWorldPoint);

    const worldLength = calculateSpatialDistance(startWorldPoint, endWorldPoint);  //计算2点之间的距离，带高度

    const windowLength = calculateWindowDistance(startPoint, endPoint);//计算2点在窗口之间的距离，无高度！
    const worldInterval = worldLength / 100.0;  //世界间隔1%  给了100个取样点
    const windowInterval = windowLength / 100.0;  //窗口间隔1%
    for (let i = 1; i < 100; i++) {  //100个点循环
        const tempWindowPoint = findWindowPositionByPixelInterval(startPoint, endPoint, windowInterval * i);  //二维点 带x,y  返回实际坐标点

        const tempPoint = findCartesian3ByDistance(startWorldPoint, endWorldPoint, worldInterval * i);   //三维点 带x,y 返回理论坐标点

        const surfacePoint = pickCartesian(this.viewer, tempWindowPoint);  //这个结果包括坐标（模型或地理），取到绝对高度或贴地高度（高程数据），取实际高度

        const tempRad = Cesium.Cartographic.fromCartesian(tempPoint);  //三维点转经纬弧度

        const surfaceRad = Cesium.Cartographic.fromCartesian(surfacePoint.cartesian); //处理后的二维点带了高度值，转经纬弧度

        if (surfaceRad.height > tempRad.height) {  //如果实际高大于理论高
            barrierPoint = tempPoint;  //把过不去的点坐标取出来，循环直接结束
            break;
        }
    }
    return barrierPoint;
}

export function convertCartesian3ToCartesian2(viewer: Cesium.Viewer, position: Cesium.Cartesian3): Cesium.Cartesian2 {
    return Cesium.SceneTransforms.wgs84ToWindowCoordinates(viewer.scene, position)   //将Cartesian3转换为windows坐标
}

export function calculateSpatialDistance(
    startPoint: Cesium.Cartesian3,
    endPoint: Cesium.Cartesian3
): number {
    return Math.sqrt(Math.pow(endPoint.x - startPoint.x, 2) + Math.pow(endPoint.y - startPoint.y, 2) + Math.pow(endPoint.z - startPoint.z, 2));  //计算两点之间的距离
}

export function calculateWindowDistance(startPoint: Cesium.Cartesian2, endPoint: Cesium.Cartesian2): number {
    return Math.sqrt(Math.pow(endPoint.y - startPoint.y, 2) + Math.pow(endPoint.x - startPoint.x, 2));
}

export function findWindowPositionByPixelInterval(startPosition: Cesium.Cartesian2, endPosition: Cesium.Cartesian2, interval: number): Cesium.Cartesian2 {  //返回的是一个坐标点
    const result = new Cesium.Cartesian2(0, 0);
    const length = Math.sqrt(Math.pow(endPosition.x - startPosition.x, 2) + Math.pow(endPosition.y - startPosition.y, 2));
    if (length < interval) {
        return result;
    } else {
        const x = (interval / length) * (endPosition.x - startPosition.x) + startPosition.x;
        //alert(interval/length)
        const y = (interval / length) * (endPosition.y - startPosition.y) + startPosition.y;
        result.x = x;
        result.y = y;
    }
    return result;
}

export function findCartesian3ByDistance(startPosition: Cesium.Cartesian3, endPosition: Cesium.Cartesian3, interval: number): Cesium.Cartesian3 {
    const result = new Cesium.Cartesian3(0, 0, 0);
    const length = Math.sqrt(Math.pow(endPosition.z - startPosition.z, 2) + Math.pow(endPosition.x - startPosition.x, 2) + Math.pow(endPosition.y - startPosition.y, 2));
    if (length < interval) {
        return result;
    } else {
        const x = (interval / length) * (endPosition.x - startPosition.x) + startPosition.x;
        //alert(interval/length)
        const y = (interval / length) * (endPosition.y - startPosition.y) + startPosition.y;
        const z = (interval / length) * (endPosition.z - startPosition.z) + startPosition.z;
        result.x = x;
        result.y = y;
        result.z = z;
    }
    return result;
}

export function pickCartesian(viewer: Cesium.Viewer, windowPosition: Cesium.Cartesian2): PickResult {
    //根据窗口坐标，从场景的深度缓冲区中拾取相应的位置，返回笛卡尔坐标。
    const cartesianModel = viewer.scene.pickPosition(windowPosition);  //笛卡尔坐标
    //场景相机向指定的鼠标位置（屏幕坐标）发射射线
    const ray = viewer.camera.getPickRay(windowPosition);
    //获取射线与三维球相交的点（即该鼠标位置对应的三维球坐标点，因为模型不属于球面的物体，所以无法捕捉模型表面）
    const cartesianTerrain = viewer.scene.globe.pick(ray, viewer.scene);  //获取的世界坐标

    const result = new PickResult();
    if (typeof (cartesianModel) !== 'undefined' && typeof (cartesianTerrain) !== 'undefined') {  //获取的值有效
        result.cartesian = cartesianModel || cartesianTerrain;
        result.CartesianModel = cartesianModel;  //模型坐标
        result.cartesianTerrain = cartesianTerrain as Cesium.Cartesian3; //地理坐标
        result.windowCoordinates = windowPosition.clone();
        //坐标不一致，证明是模型，采用绝对高度。否则是地形，用贴地模式。
        result.altitudeMode = cartesianModel.z.toFixed(0) !== cartesianTerrain!.z.toFixed(0) ? Cesium.HeightReference.NONE : Cesium.HeightReference.CLAMP_TO_GROUND; //高度模式
    }
    return result;
}
