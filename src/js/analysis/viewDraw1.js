import ViewShed from './viewshed1.js'
//import PlotDrawTip from "../../utils/PlotDrawTip"
//import PlotDrawTip from "@/utils/PlotDrawTip.js"
class viewDraw {
  constructor(viewer, handler) {
    this.viewer = viewer;
    this.handler = handler
    this.startPosition = undefined
    this.endPosition = undefined
    this.i = 0
    this.drawViewshedEntity = null
  }

  //激活
  activate() {

    this.deactivate();
    this.registerEvents(); //注册鼠标事件
    this.viewer.enableCursorStyle = true;
    this.viewer._element.style.cursor = 'crosshair';
    this.viewer.enableCursorStyle = true;
    this.viewer._element.style.cursor = 'crosshair';
    //this.plotDrawTip = new PlotDrawTip(this.viewer);
    //this.plotDrawTip.setContent(['左键点击开始绘制'])
  }

  //禁用
  deactivate() {
    this.i = 0
    this.drawEntity = undefined;
    this.endPosition = undefined
    this.startPosition = undefined
    this.viewer._element.style.cursor = 'default';
    this.viewer.enableCursorStyle = true;
    this.unRegisterEvents();
    //if (!this.plotDrawTip) return;
    //this.plotDrawTip.remove();
    //this.plotDrawTip = undefined;
  }

  //注册鼠标事件
  registerEvents() {
    this.leftClickEvent();
    //this.mouseMoveEvent();
  }

  leftClickEvent() {

    let that = this;
    //单击鼠标左键画点
    this.handler.setInputAction(e => {
      //alert('第一次点击');
      that.i++

      //第一次点击
      if (that.i == 1) {

        console.log('第一次点击');
        //let startPosition = that.viewer.scene.pickPosition(e.position);
        let startPosition = that.getCatesian3FromPX(e.position, that.viewer);

        if (startPosition && Cesium.defined(startPosition)) {
          that.startPosition = startPosition
          //this.plotDrawTip.setContent(['左键点击结束点，完成绘制'])
          console.log('start' + startPosition);
          that.drawViewshedEntity = new ViewShed(that.viewer, {
            viewPosition: that.startPosition,
            viewPositionEnd: that.startPosition,
          })
        }
      }
      that.mouseMoveEvent();
      //第二次点击
      if (that.i == 2) {

        //let endPosition = that.viewer.scene.pickPosition(e.position);
        endPosition = that.getCatesian3FromPX(e.position, that.viewer);
        console.log('end1' + endPosition);
        if (endPosition && Cesium.defined(endPosition)) {
          console.log('end2' + endPosition);
          that.endPosition = endPosition

          that.drawViewshedEntity.updatePosition(that.endPosition)
          console.log('end3' + endPosition);
          that.drawViewshedEntity.update()
        }
        console.log('end4' + endPosition);
        that.deactivate()
      }

    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
  }

  mouseMoveEvent() {
    //alert('moveEvent111');
    let that = this;
    this.handler.setInputAction(e => {
      console.log('moveEvent111');
      //that.endPosition = that.viewer.scene.pickPosition(e.endPosition)
      if (that.i == 1) {
        //alert('moveEvent111');
        //let endPosition = that.getCatesian3FromPX(e.position, that.viewer);
        let endPosition = that.viewer.scene.pickPosition(e.endPosition)
        if (endPosition && Cesium.defined(endPosition)) {
          console.log(endPosition);
          that.endPosition = endPosition;
        }
        //alert(that.endPosition);
        if (!that.endPosition) return
        //this.plotDrawTip && this.plotDrawTip.updatePosition(this.endPosition);
        //if (that.i == 1) {
        if (that.drawViewshedEntity) {
          //alert('moveEvent');
          that.drawViewshedEntity.updatePosition(that.endPosition)
          that.drawViewshedEntity.update()

        }
      }
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
  }

  //解除鼠标事件
  unRegisterEvents() {
    this.handler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK);
    this.handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE);
  }

  getCatesian3FromPX(px, viewer) {

    var picks = viewer.scene.drillPick(px);
    viewer.scene.render();
    console.info('getCatesian3FromPX2222!');
    var cartesian;
    var isOn3dtiles = false;
    for (var i = 0; i < picks.length; i++) {
      if ((picks[i] && picks[i].primitive) && picks[i].primitive instanceof Cesium.Cesium3DTileset) { //模型上拾取
        isOn3dtiles = true;
        break;
      }
    }
    if (isOn3dtiles) {
      cartesian = viewer.scene.pickPosition(px);
    } else {
      var ray = viewer.camera.getPickRay(px);
      if (!ray) return null;
      cartesian = viewer.scene.globe.pick(ray, viewer.scene);
    }
    //alert('getCatesian3FromPX3333!');
    return cartesian;
  }


  //这里可以根据业务调配
  viewshedInit(ele) {
    const model = ele
    const style = model.properties.style
    this.drawViewshedEntity = new ViewShed(this.viewer, {
      viewPosition: style.viewPosition,
      viewDistance: style.viewDistance,
      viewPositionEnd: style.viewPositionEnd,
      horizontalViewAngle: style.horizontalViewAngle,
      verticalViewAngle: style.verticalViewAngle,
      viewPitch: style.viewPitch,
      viewHeading: style.viewHeading,
      visibleAreaColor: style.visibleAreaColor,
      invisibleAreaColor: style.invisibleAreaColor,
      isSketch: style.isSketch
    })
    this.drawViewshedEntity.update()
    //保存到全局数组中方便删除
    global.toolsList.push({ relevantId: model.relevantId, viewShed: this.drawViewshedEntity })
  }
}

export default viewDraw;
