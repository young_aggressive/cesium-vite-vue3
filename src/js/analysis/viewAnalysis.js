//import { createTypeReferenceDirectiveResolutionCache } from "typescript";
import ViewShed from "./ViewShed1.js";
import ViewDraw from "./viewDraw1.js";
import ViewShedAnalysis from "./CreateViewershed.js";
/**
 * 量算控制类
 * @description 量算控制类，通过此类对象，可进行不同类型的量算操作，而不用多次new 不同类型的量算对象。
 * @class
 */


// var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
//     if (Object.defineProperty) {
//         Object.defineProperty(cooked, "raw", {
//             value: raw
//         });
//     } else {
//         cooked.raw = raw;
//     }
//     return cooked;
// };
// var glsl = function (x) {
//     return x.toString();
// };

class ViewTool {
    /**
     * @param {Cesium.viewer} viewer 地图viewer对象
     * @param {Object} obj 基础配置 
     */
    constructor(viewer, obj) {    //构造函数
        if (!viewer) {
            console.warn("缺少必要参数！--viewer");
            return;
        }
        obj = obj || {};
        this.viewer = viewer;

        this.vaObj = new ViewShedAnalysis(this.viewer, "canvasMap");

        //地图场景
        // var scene = this.viewer.scene;
        // scene.screenSpaceCameraController.enableRotate = true;
        // scene.screenSpaceCameraController.enableTranslate = true;
        // scene.screenSpaceCameraController.enableZoom = true;
        // scene.screenSpaceCameraController.enableTilt = true;
        // scene.screenSpaceCameraController.enableLook = true;
        this.viewer.scene.globe.shadows = Cesium.ShadowMode.ENABLED;


        //视域点集合
        // this.viewPoints = [];
        // this.viewPoint = null;

        this.Vpoint = 0; //观察点
        this.Gpoint = 0; //远程点

        // this.destPoints = [];
        // this.destPoint = null;

        this.iLength = 0; //已经读取的视域点
        this.jLength = 0; //已经读取的目标点

        /**
         * @property {Object} nowDrawMeasureObj 当前测量对象
         */
        this.nowDrawMeasureObj = null;

        /**
         * @property {Array} measureObjArr 测量对象数组
         */
        this.measureObjArr = [];
        this.nowEditMeasureObj = null;
        this.handler = null;

        this.viewAnalysis = null;//可视域对象
        //this.flag = 1; //添加面板


        /**
         * @property {Boolean} [canEdit=true] 测量对象是否可编辑
         */
        this.canEdit = obj.canEdit == undefined ? true : obj.canEdit;

        /**
         * @property {Boolean} [intoEdit=true] 绘制完成后，是否进入编辑状态（当canEdit==true，才起作用）
         */
        this.intoEdit = obj.intoEdit == undefined ? true : obj.intoEdit;
        this.bindEdit();

        /**
         * @property {Object} nowDrawMeasureObj 当前绘制对象，绘制完成后为undifined
         */
        this.nowDrawMeasureObj = undefined;

        /**
         * @property {Object} nowEditMeasureObj 当前编辑对象，编辑完成后为undifined
         */
        this.nowEditMeasureObj = undefined;

    }

    /**
    * 绑定编辑
    */
    bindEdit() {
        let that = this;
        // 如果是线 面 则需要先选中
        if (!this.handler) this.handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
        this.handler.setInputAction(function (evt) {
            if (!that.canEdit) return;
            // 若当前正在绘制 则无法进行编辑操作
            if (that.nowDrawMeasureObj) return;
            let pick = that.viewer.scene.pick(evt.position); //重要
            if (Cesium.defined(pick) && pick.id && pick.id.objId) {
                // 选中实体
                for (let i = 0; i < that.measureObjArr.length; i++) {
                    if (
                        pick.id.objId == that.measureObjArr[i].objId &&
                        (that.measureObjArr[i].state == "endCreate" ||
                            that.measureObjArr[i].state == "endEdit")
                    ) {
                        // 结束上一个编辑
                        if (that.nowEditMeasureObj) {
                            // 结束除当前选中实体的所有编辑操作
                            that.nowEditMeasureObj.endEdit();
                            if (that.endEditFun) that.endEditFun(that.nowEditMeasureObj);
                            that.nowEditMeasureObj = undefined;
                        }
                        // 开始当前编辑
                        that.measureObjArr[i].startEdit();
                        that.nowEditMeasureObj = that.measureObjArr[i];
                        if (that.startEditFun) that.startEditFun(that.nowEditMeasureObj); // 开始编辑
                        break;
                    }
                }
            } else {
                // 未选中实体 则结束编辑
                if (that.nowEditMeasureObj) {
                    that.nowEditMeasureObj.endEdit();
                    if (that.endEditFun) that.endEditFun(that.nowEditMeasureObj); // 结束事件
                    that.nowEditMeasureObj = undefined;
                }
            }
        }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
    }

    /**
     * 开始分析
     * @param {Object} opt 
     * @param {Number} opt.type 分析类型（1~添加视域点/2~添加目标点/3~移动实体/4~通视分析） 对应4个按钮
     */
    start(opt) {
        opt = opt || {};
        if (!opt.type) return;
        switch (Number(opt.type)) {
            case 1: // 添加观察点
                alert('绘制观察点，右键结束!');
                this.addVPoint1();
                //ms = new MeasureSpaceDistance(this.viewer, opt);
                break;
            case 2: // 添加目标点
                alert('绘制远端点，右键结束!');
                this.addGPoint1();
                //ms = new MeasureGroundDistance(this.viewer, opt);
                break;
            case 3: // 移动实体
                alert('移动实体开始，右键结束!');
                movePoint();
                //ms = new MeasureSpaceArea(this.viewer, opt);
                break;
            case 4: // 视域分析
                //alert('视域分析开始');
                try {
                    //this.visibleAnalysis();
                    this.startAnalysis();
                } catch (error) {
                    alert(error);
                }
                //alert('visibleAnalysis!!!!!!');
                //ms = new MeasureHeight(this.viewer, opt);
                break;
            case 5:
                alert('重新开始');
                this.clear();
            default:
                break;
        }

    }

    startAnalysis() {
        this.vaObj.createViewshed(10);
    }

    clear() {
        this.vaObj.clearAll();
    }

    //添加观察点
    addVPoint1() {
        if (this.handler)
            this.handler.destroy();
        let that = this;
        this.handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
        this.handler.setInputAction(function (e) {
            //var position = this.viewer.scene.pickPosition(e.position);

            var position = that.getCatesian3FromPX(e.position, that.viewer);
            //alert('222222!');
            //将笛卡尔坐标转化为经纬度坐标
            var cartographic = Cesium.Cartographic.fromCartesian(position);  //笛卡尔转弧度
            var longitude = Cesium.Math.toDegrees(cartographic.longitude); //弧度转度
            var latitude = Cesium.Math.toDegrees(cartographic.latitude);
            var height = Math.ceil(that.viewer.camera.positionCartographic.height);//这块儿好像不对，应该是这个点的高程吧？？？？？？？ 这个是鼠标的高度
            var toPoint = new Cesium.Cartesian3.fromDegrees(longitude, latitude, height); //经纬度转弧度
            //toPoint = webMercatorProjection.unproject(toPoint);       //转为投影坐标，但是没有用

            //that.viewPoints.push(toPoint);
            that.Vpoint = position;

            //alert(position + "\n" + toPoint);

            // that.viewPoint = that.viewer.entities.add({ //添加实体
            //     //parent: that.viewer.entities.add(new Cesium.Entity()),
            //     position: new Cesium.Cartesian3.fromDegrees(longitude, latitude, height),
            //     name: 'vpoint',
            //     ellipsoid: {
            //         radii: new Cesium.Cartesian3(50, 50, 50),
            //         material: Cesium.Color.RED,
            //     },
            // });
            that.createLabel(position, "观察点");
            that.viewPoint = that.createellipsoid(position, 'vpoint');
            //alert("viewPoint" + that.viewPoint);

        }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
        //alert('addvpoint1!');
        //单击鼠标右键结束画点
        this.handler.setInputAction(function (movement) {
            that.handler.destroy();
            alert('已结束观察点添加功能');
        }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
    }

    //添加远程点
    addGPoint1() {

        if (this.handler)
            this.handler.destroy();
        let that = this;
        this.handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
        this.handler.setInputAction(function (e) {
            var position = that.getCatesian3FromPX(e.position, that.viewer);
            //将笛卡尔坐标转化为经纬度坐标
            var cartographic = Cesium.Cartographic.fromCartesian(position);
            var longitude = Cesium.Math.toDegrees(cartographic.longitude);
            var latitude = Cesium.Math.toDegrees(cartographic.latitude);
            var height = Math.ceil(that.viewer.camera.positionCartographic.height);
            var toPoint = new Cesium.Cartesian3.fromDegrees(longitude, latitude, height);
            //toPoint = webMercatorProjection.unproject(toPoint);
            //that.destPoints.push(toPoint);
            that.Gpoint = position;

            that.createLabel(position, "远程点");
            that.destPoint = that.createellipsoid(position, 'gpoint');

        }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
        //单击鼠标右键结束画点
        this.handler.setInputAction(function (movement) {
            that.handler.destroy();
            alert('已结束远程点添加功能');
        }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
    }

    //兼容模型和地形上坐标拾取
    getCatesian3FromPX(px, viewer) {

        var picks = viewer.scene.drillPick(px);
        viewer.scene.render();
        //alert('getCatesian3FromPX2222!');
        var cartesian;
        var isOn3dtiles = false;
        for (var i = 0; i < picks.length; i++) {
            if ((picks[i] && picks[i].primitive) && picks[i].primitive instanceof Cesium.Cesium3DTileset) { //模型上拾取
                isOn3dtiles = true;
                break;
            }
        }
        if (isOn3dtiles) {
            cartesian = viewer.scene.pickPosition(px);
        } else {
            var ray = viewer.camera.getPickRay(px);
            if (!ray) return null;
            cartesian = viewer.scene.globe.pick(ray, viewer.scene);
        }
        //alert('getCatesian3FromPX3333!');
        return cartesian;
    }



    createellipsoid(position, name) {
        //alert("ccccccc");
        if (!position) return;
        let ent = null;
        switch (name) {
            case "vpoint": // 添加视域点
                ent = this.viewer.entities.add({
                    position: position,
                    name: name,
                    ellipsoid: {
                        radii: new Cesium.Cartesian3(50, 50, 50),
                        material: Cesium.Color.BLUE,
                        //distanceDisplayCondition: new Cesium.DistanceDisplayCondition(100.0, 20000.0)
                    },
                });
                break;
            case "gpoint": // 添加目标点
                ent = this.viewer.entities.add({
                    position: position,
                    name: name,
                    ellipsoid: {
                        radii: new Cesium.Cartesian3(50, 50, 50),
                        material: Cesium.Color.BLACK,
                        //distanceDisplayCondition: new Cesium.DistanceDisplayCondition(100.0, 20000.0)
                    },
                });
                break;
            default:
                break;
        }
        return ent;
    }

    createLabel(c, text) {
        if (!c) return;
        return this.viewer.entities.add({
            position: c,
            label: {
                text: text || "",
                font: '18px Helvetica',
                fillColor: Cesium.Color.WHITE,
                outlineColor: Cesium.Color.BLACK,
                outlineWidth: 2,
                disableDepthTestDistance: Number.POSITIVE_INFINITY,
                style: Cesium.LabelStyle.FILL_AND_OUTLINE,
                pixelOffset: new Cesium.Cartesian2(0, -20)
            }
        });
    }

    //分析代码
    visibleAnalysis1() {
        if (this.Vpoint && this.Gpoint) {
            try {
                var options = {
                    viewPosition: this.Vpoint,
                    viewer: this.viewer,
                    direction: Cesium.Cartesian3.normalize(Cesium.Cartesian3.subtract(this.Vpoint,
                        this.Gpoint,
                        new Cesium.Cartesian3()), new Cesium.Cartesian3()),
                    pitch: 0,
                    visualRange: Cesium.Cartesian3.distance(this.Vpoint, this.Gpoint)
                };
                var distance = Cesium.Cartesian3.distance(this.Vpoint, this.Gpoint);
                console.log("距离：", distance, "米");
                this.viewAnalysis = new ViewShed(options); //这里是重要的
            } catch (error) {
                alert(error);
            }
        } else
            alert('视点或目标点未选择');

    }

    visibleAnalysis() {
        //if (this.Vpoint && this.Gpoint) {
        try {
            // var options = {
            //     viewPosition: this.Vpoint,
            //     viewer: this.viewer,
            //     direction: Cesium.Cartesian3.normalize(Cesium.Cartesian3.subtract(this.Vpoint,
            //         this.Gpoint,
            //         new Cesium.Cartesian3()), new Cesium.Cartesian3()),
            //     pitch: 0,
            //     visualRange: Cesium.Cartesian3.distance(this.Vpoint, this.Gpoint)
            // };
            // var distance = Cesium.Cartesian3.distance(this.Vpoint, this.Gpoint);
            // console.log("距离：", distance, "米");

            this.viewAnalysis = new ViewDraw(this.viewer, this.handler);

            this.viewAnalysis.activate();

        } catch (error) {
            alert(error);
        }
        // } else {
        //     alert('视点或目标点未选择');
        // }

        //alert('visibleAna1ysis');

    }


}

export default ViewTool;