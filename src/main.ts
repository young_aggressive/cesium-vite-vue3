import { createApp } from 'vue'
import App from './index1.vue'
//import App from './viewershed.vue'
import Antd from "ant-design-vue";
import "ant-design-vue/dist/antd.css";
import ElementPlus from 'element-plus'  //npm install element-plus --save 之后
import 'element-plus/dist/index.css'
// import lang from 'vue-cesium/lang/zh-hans'
// import VueCesium from 'vue-cesium'
// import '@/assets/styles/cesium-widgets.css'

const app = createApp(App)
app.use(Antd).use(ElementPlus)


//===================引入3d平台相关 start=========================
// 1、引入字体库
import '@/assets/font/iconfont.css'

// 2、引入核心类库vis3d.js
//import "vis3d/vis3d.css"

import vis3d from "@/js/vis3d.js";
window.vis3d = vis3d;
/**应该是要自己搞定vis3d中的功能，所以这个js不用引入
import vis3d from 'vis3d/vis3d.js'

Vue.prototype.vis3d = window.vis3d = vis3d;  //引入全局变量 Prototype(原型属性) Windows(全局变量)  https://blog.csdn.net/qq_40259641/article/details/90635778  vue2中的用法

//vue3中的搞法 main.ts  取消了this
app.config.globalProperties.name = '沐华'

//其他组件中调用
const global = appContext.config.globalProperties
console.log(global.name) // 沐华
*/

// 3、全局引入自定义拖拽组件，用到再弄
// import Card from "@/views/map3d/components/card/Card.vue";
// Vue.component("Card", Card);

//4、主题样式设置，探索中...
app.config.globalProperties.toolStyle = {
    themeType: "drak", // 主题样式颜色 dark（暗色）、blue（科技蓝）、green（生态绿）
    toolsType: 'default' // 右侧工具条类型 default（条状工具条） 、dropdown（下拉工具）、fade（淡入工具）
}

//5、打印，先不用
// import Print from 'vue-print-nb'
// app.use(Print);
//===================引入3d平台相关 end=========================

app.mount('#app')

