import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import cesium from "vite-plugin-cesium";
//引入path模块
import path from "path";

//创建一个文件读取方法
// function resolve(url) {
//   return path.resolve(__dirname, url)
// }

const resolve = (dir) => path.join(__dirname, dir)

// https://vitejs.dev/config/
// export default defineConfig({
//   plugins: [vue()],
//   resolve: {
//     alias: {
//       '@': fileURLToPath(new URL('./src', import.meta.url))
//     }
//   }
// })






export default defineConfig({
  //配置@别名
  resolve: {
    alias: {
      "@": resolve('./src'),
      "@netWork": resolve("./src/network"),
      "@utils": resolve("./src/utils"),
    }
  },
  plugins: [vue(), cesium()],
})
